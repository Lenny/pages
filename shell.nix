{ pkgs ? import <nixpkgs> { }
}: pkgs.mkShell {
  inputsFrom = [ (pkgs.haskellPackages.callCabal2nix "pages" ./. { }) ];
  buildInputs = [
    pkgs.zlib
  ];
  nativeBuildInputs = [
    pkgs.haskell-language-server
    pkgs.cabal-install
    pkgs.ormolu
  ];
}
